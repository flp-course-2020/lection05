package lection.type_constructors;

public class Step04JavaArrays {

  public static final void assignment() {
    String[] strings = {"str"};
    Object[] objects = strings;
    objects[0] = new Integer(1);
    String s = strings[0];
  }

  public final static void main(String[] args) {
    assignment();
  }

  public void sort(Object[] xs, java.util.Comparator cmp) {
    // sorting...
  }
}
