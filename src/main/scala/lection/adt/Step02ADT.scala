package lection.adt

object Step02Example1 {

  sealed trait A
  final case class B() extends A
  final case class C() extends A

}
