package lection.type_constructors

object Step03Example1 {

  // var xs: List = null

}

object Step03Example2 {

  sealed trait List[+A]

  case class Cons[A](head: A, tail: List[A]) extends List[A]

  case class Nil[A]() extends List[A]



  val xs: List[Any] = Cons(1, Cons(2, Nil()))
}

object Step03Example3 {

  case class Box[A](var value: A)

  // val c1: Box[String] = Box("str")
  // val c2: Box[Any] = c1
  // c2.value = 1
  // val string: String = c1.value
}
